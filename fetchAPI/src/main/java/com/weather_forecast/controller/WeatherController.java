package com.weather_forecast.controller;

import com.weather_forecast.entity.WeatherData;
import com.weather_forecast.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/weather-forecast")
public class WeatherController {
    private static final String API_KEY = "2b98009d8f5f93ea11e6868e42be8538";
    @Autowired
    private WeatherService weatherService;
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/data/{city}")
    public String getWeatherData(@PathVariable("city") String city) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange("https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&units=metric&cnt=16&appid=" + API_KEY, HttpMethod.GET, entity, String.class).getBody();

    }

    @PostMapping("/")
    public WeatherData saveData(@RequestBody WeatherData weatherData) {
        return weatherService.saveWeatherInfo(weatherData);
    }

    @GetMapping("/all")
    public List<WeatherData> getAllCityData() {
        return weatherService.getAllCityData();
    }

    //    @GetMapping("/{city}")
//    public WeatherData getWeatherForCity(@PathVariable("city") String city) {
//        return weatherService.getWeatherForCity(city);
//    }
    @GetMapping("/getCityByName/{city}")
    public WeatherData getWeatherByCityName(@PathVariable String city) {
        return weatherService.getWeatherByCityName(city);
    }
    @GetMapping("/getAveragePressure")
    public double getAveragePressure(){
        return weatherService.getAveragePressure();
    }
    @GetMapping("/getAverageTemperature")
    public double getAverageTemperature(){
        return weatherService.getAverageTemperature();
    }
}
