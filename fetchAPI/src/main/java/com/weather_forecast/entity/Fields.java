package com.weather_forecast.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "main")
public class Fields {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int fieldsId;
    private Long dt;
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "Vals_ID")
    @Embedded
    private Vals main;
    @ManyToOne
    private WeatherData weatherData;
}
