package com.weather_forecast.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@Table(name = "vals")
//@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Vals {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
    private double temp;
    private double temp_min;
    private double temp_max;
    private double pressure;
}