package com.weather_forecast.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class City {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long uniqueId;
//    private Long id;
    private String name;
    private Long timezone;
//    @OneToOne(mappedBy = "city")
//    @JoinColumn(name = "weather_data_id")
//    private WeatherData weatherData;
}
