package com.weather_forecast.repository;

import com.weather_forecast.entity.WeatherData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherData, String> {
//    WeatherData findByName(String name);
//    @Query(value = "SELECT * FROM CITY WHERE NAME=?1",nativeQuery = true)
//    WeatherData findByName(String name);

    @Query(value = "SELECT avg (PRESSURE) FROM MAIN m",nativeQuery = true)
    double getAveragePressure();

    @Query(value = "SELECT avg (TEMP) FROM MAIN m",nativeQuery = true)
    double getAverageTemperature();

    WeatherData findByCityName(String city);
}
