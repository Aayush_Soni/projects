package com.weather_forecast.service;

import com.weather_forecast.entity.WeatherData;
import com.weather_forecast.repository.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Service
public class WeatherService {
    @Autowired
    private WeatherRepository weatherRepository;

    public WeatherData saveWeatherInfo(WeatherData weatherData){
        return weatherRepository.save(weatherData);
    }

    public List<WeatherData> getAllCityData(){
        return weatherRepository.findAll();
    }

    public WeatherData getWeatherByCityName(String city) {
        return weatherRepository.findByCityName(city);
    }

    public double getAveragePressure(){
        return weatherRepository.getAveragePressure();
    }
    public double getAverageTemperature(){
        return weatherRepository.getAverageTemperature();
    }
//    public WeatherData getWeatherForCity(String name){
//        return weatherRepository.findByName(name);
//    }

}
