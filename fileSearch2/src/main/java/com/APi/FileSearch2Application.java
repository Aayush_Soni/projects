package com.APi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
public class FileSearch2Application {

	public static void main(String[] args) {
		SpringApplication.run(FileSearch2Application.class, args);
	}

}
