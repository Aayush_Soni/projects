package com.APi.controller;

import com.APi.entity.ImageEntity;
import com.APi.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/scanner")
public class FileController {
    @Autowired
    private FileService fileService;

    @PostMapping("/PostAllFiles")
    public ImageEntity saveImage() throws IOException {
        return fileService.saveImage();
    }

    @GetMapping("/search/getAllFiles")
    public List<ImageEntity> getAllFiles(){
        return fileService.getAllFiles();
    }

    @GetMapping("/search/byName/{name}")
    public List<ImageEntity> findByName(@PathVariable("name") String name){
        return fileService.findByName(name);
    }

    @GetMapping("/search/byType/{type}")
    public List<ImageEntity> findByType(@PathVariable("type")String type){
        return fileService.findByType(type);
    }

    @GetMapping("/search/byDate/{date}")
    public List<ImageEntity> findByDate(@PathVariable("date")String date){
        return fileService.findByDate(date);
    }


}
