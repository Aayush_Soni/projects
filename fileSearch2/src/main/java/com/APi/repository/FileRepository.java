package com.APi.repository;

import com.APi.entity.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<ImageEntity,Long> {
    List<ImageEntity> findByName(String name);

    List<ImageEntity> findByType(String type);

    List<ImageEntity> findByDate(String date);
}
