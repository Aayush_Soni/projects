package com.APi.service;

import com.APi.entity.ImageEntity;
import com.APi.repository.FileRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@Service
public class FileService {
    @Autowired
    private FileRepository fileRepository;

//    Directory directory = new javaxt.io.Directory("");
//    directory.getFiles();
//    javaxt.io.File[] files;

    public File directory =new File("C:\\fileSearch2Folder");

    public ImageEntity saveImage() throws IOException {

        int id=1;
        File[] files = directory.listFiles();
        for (File file: files) {
            ImageEntity image= new ImageEntity();
            if (file!=null && (file.getName().toLowerCase().endsWith(".jpg"))|| file.getName().toLowerCase().endsWith(".png")){

                image.setName(file.getName());
                image.setType(FilenameUtils.getExtension(file.getName()));
                image.setId(id++);

                SimpleDateFormat simpleDateFormat= new SimpleDateFormat("dd-MM-yyyy");
                BasicFileAttributes attr = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                Date date = new Date(attr.creationTime().to(TimeUnit.MILLISECONDS));
                String formattedDate = simpleDateFormat.format(date);

                image.setDate(formattedDate.toString());
            }
            fileRepository.save(image);
        }
        return null;
    }

    public List getAllFiles() {
        List<String> listOfFiles = new ArrayList<>();

        String[] list = directory.list();
        for (String s : list) {
            listOfFiles.add(s);
        }
        return listOfFiles;
    }

    public List<ImageEntity> findByName(String name){
        List<ImageEntity> images= fileRepository.findByName(name);
        return images;
    }

    public List<ImageEntity> findByType(String type){
        List<ImageEntity> images = fileRepository.findByType(type);
        return images;
    }

    public List<ImageEntity> findByDate(String date){
        List<ImageEntity> images = fileRepository.findByDate(date);
        return images;
    }


}
